﻿using EmailAlertComponent;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AutomateEmailAlerts
{
    class Program
    {
        //private static System.Diagnostics.EventLog eventLogEmailAlert1;
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            if ((int)DateTime.UtcNow.DayOfWeek == 6)
            //if ((int)DateTime.UtcNow.DayOfWeek == 5)
            {
                if (DateTime.UtcNow.Hour == 10)
                   // if (DateTime.UtcNow.Hour == 5)
                {
                    Log.Info("Thumbay: Invoked Email Alert method");
                    ServiceMailMethod_Thumbay();
                    Log.Info("Thumbay: Exit Email Alert method");
                }
            }

            if (DateTime.UtcNow.Hour == 3)
            //if (DateTime.UtcNow.Hour == 4)
            {
                Log.Info("RAK Communication:Invoked Email Alert method");
                ServiceMailMethod_RAK();
                Log.Info("RAK Communication:Exit Email Alert method");

            }
            if (DateTime.UtcNow.Hour == 3)
            //if (DateTime.UtcNow.Hour == 5)
            {
                Log.Info("David: Invoked Email Alert method");
                ServiceMailMethod_temp();
                Log.Info("David: Exit Email Alert method");
            }

            //ServiceMailMethod_Thumbay();
        }

        /// <summary>
        /// Daily Report for Thumbay
        /// </summary>
        //public static void ServiceMailMethod_Thumbay()
        //{
        //    string contests;

        //    contests = "<table style='width:100%;border-collapse: collapse; '>";
        //    contests += "<thead style='background: #444; color: white;font-weight:bold;' ><tr><th>TerminalID</th><th>Reg No</th><th>Department</th><th>Date</th><th>Distance(km)</th></tr></td>";
        //    var idx = 0;

        //    //GpsMail.GetEmailIdsFromDB getEmails = new GpsMail.GetEmailIdsFromDB();
        //    EmailAlertComponent.GetData getdata = new EmailAlertComponent.GetData();
        //    getdata.connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;
        //    //For getting offline vehicle details
        //    getdata.storedProcName = "sp_data_emailreport_new";
        //    System.Data.DataSet ds = getdata.GetSPData(0);//for thumbay department
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        try
        //        {
        //            foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
        //            {
        //                bool even = idx % 2 == 0;
        //                if (even)
        //                {
        //                    contests += "<tr style='background: #eee;'>";
        //                }
        //                else
        //                {
        //                    contests += "<tr style='background: #ffe;'>";
        //                }
        //                contests += "<tr style='background: #eee;'>";
        //                contests += "<td style='padding: 6px;color:#000; 	border: 1px solid #ccc; text-align: left;' >" + dr["TerminalID"] + "</td>";
        //                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["RegNo"] + "</td>";
        //                contests += "<td style='padding: 6px; 	color:#000;border: 1px solid #ccc; text-align: left;'>" + dr["Name"] + "</td>";
        //                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + Convert.ToDateTime(dr["Date"]).ToString("dd-MMM-yyyy") + "</td>";
        //                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["Distance"] + "</td>";
                        
        //                DateTime TodayDate = DateTime.Now.Date;
        //                DateTime RunningDate = Convert.ToDateTime(dr["Date"]);
        //                if (TodayDate < RunningDate)
        //                {
        //                    // Contents -------------
        //                }

        //                contests += "</tr>";
        //                idx++;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.Error(ex.Message + " :Thumbay");
        //        }

        //        contests += "</table>";
        //        string mail_text = "<p style='text-align:left;'>Hi,</p>";
        //        mail_text = mail_text + "<p style='text-align:left;'> Daily Report  details for (" + DateTime.Now.Date.AddDays(-1).ToString("dd-MMM-yyyy") + ") are listed out in the following table. </p>";
        //        mail_text += "<div >" + contests + "</div><br>";
        //        //mail_text += "<p>Order registered by <b>" + db.users.FirstOrDefault(a => a.Id == SessionWrapper.UserID).Name + "</b></p>";
        //        mail_text += "<div>";
        //        mail_text += "<h4>Regards,</h4>";

        //        mail_text += "<p><strong>Thinture Technologies Pvt Ltd.,</strong><br>";

        //        mail_text += "";
        //        mail_text += "</div>";
        //        try
        //        {
        //            getdata.storedProcName = "sp_data_getemails";
        //            System.Data.DataSet email_ds = getdata.GetSPData(0);//- Thumbay dept Email id
        //            if (email_ds.Tables[0].Rows.Count > 0)
        //            {
        //                SendEmail(email_ds, "Daily Report", mail_text, "First", true);
        //            }
        //            Log.Info("Thumbay: Mail sent successfully!");
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.Error("Thumbay: " + ex.Message.ToString());
        //        }
        //    }
        //    else
        //    {
        //        Log.Info("Thumbay: No data available");
        //    }
        //}








        public static void ServiceMailMethod_Thumbay()
        {
            string contests;
            contests = "<table style='width:100%;border-collapse: collapse; '>";
            contests += "<thead style='background: #444; color: white;font-weight:bold;' ><tr><th>TerminalID</th><th>Reg No</th><th>Department</th><th>Date</th><th>Distance(km)</th></tr></td>";
            var idx = 0;

            //GpsMail.GetEmailIdsFromDB getEmails = new GpsMail.GetEmailIdsFromDB();
            EmailAlertComponent.GetData getdata = new EmailAlertComponent.GetData();
            getdata.connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;
            //For getting offline vehicle details
            getdata.storedProcName = "sp_data_emailreport_new";
            System.Data.DataSet ds = getdata.GetSPData(0);//for thumbay department
            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                    {
                   
                        if (!string.IsNullOrEmpty(dr["Date"].ToString()))
                        {
                            DateTime TodayDate = DateTime.Now.Date.AddDays(-1);
                            //DateTime TodayDate = Convert.ToDateTime(dr["Date"]);
                            DateTime RunningDate = Convert.ToDateTime(dr["Date"]);
                            if (TodayDate > RunningDate)
                            {
                                // Contents -------------
                                bool even = idx % 2 == 0;
                                if (even)
                                {
                                    contests += "<tr style='background: #eee;'>";
                                }
                                else
                                {
                                    contests += "<tr style='background: #ffe;'>";
                                }
                                contests += "<tr style='background: #eee;'>";
                                contests += "<td style='padding: 6px;color:#000; 	border: 1px solid #ccc; text-align: left;' >" + dr["TerminalID"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["RegNo"] + "</td>";
                                contests += "<td style='padding: 6px; 	color:#000;border: 1px solid #ccc; text-align: left;'>" + dr["Name"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + TodayDate.ToString("dd-MMM-yyyy") + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'> 0 </td>";

                                contests += "</tr>";
                                idx++;
                            }
                            else
                            {
                                bool even = idx % 2 == 0;
                                if (even)
                                {
                                    contests += "<tr style='background: #eee;'>";
                                }
                                else
                                {
                                    contests += "<tr style='background: #ffe;'>";
                                }
                                contests += "<tr style='background: #eee;'>";
                                contests += "<td style='padding: 6px;color:#000; 	border: 1px solid #ccc; text-align: left;' >" + dr["TerminalID"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["RegNo"] + "</td>";
                                contests += "<td style='padding: 6px; 	color:#000;border: 1px solid #ccc; text-align: left;'>" + dr["Name"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + Convert.ToDateTime(dr["Date"]).ToString("dd-MMM-yyyy") + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["Distance"] + "</td>";

                                contests += "</tr>";
                                idx++;
                            }   
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message + " :Thumbay");
                }

                contests += "</table>";
                string mail_text = "<p style='text-align:left;'>Hi,</p>";
                mail_text = mail_text + "<p style='text-align:left;'> Daily Report details for (" + DateTime.Now.Date.AddDays(-1).ToString("dd-MMM-yyyy") + ") are listed out in the following table. </p>";
                mail_text += "<div >" + contests + "</div><br>";
                //mail_text += "<p>Order registered by <b>" + db.users.FirstOrDefault(a => a.Id == SessionWrapper.UserID).Name + "</b></p>";
                mail_text += "<div>";
                mail_text += "<h4>Regards,</h4>";

                mail_text += "<p><strong>Thinture Technologies Pvt Ltd.,</strong><br>";

                mail_text += "";
                mail_text += "</div>";
                try
                {
                    getdata.storedProcName = "sp_data_getemails";
                    System.Data.DataSet email_ds = getdata.GetSPData(0);//- Thumbay dept Email id
                    if (email_ds.Tables[0].Rows.Count > 0)
                    {
                        SendEmail(email_ds, "Daily Report", mail_text, "First", true);
                    }
                    Log.Info("Thumbay: Mail sent successfully!");
                }
                catch (Exception ex)
                {
                    Log.Error("Thumbay: " + ex.Message.ToString());
                }
            }
            else
            {
                Log.Info("Thumbay: No data available");
            }
        }












        /// <summary>
        /// Daily Report for R.A.K Communications
        /// created by prakash on 14-feb17
        /// </summary>
        public static void ServiceMailMethod_RAK()
        {
            string contests;

            contests = "<table style='width:100%;border-collapse: collapse; '>";
            contests += "<thead style='background: #444; color: white;font-weight:bold;' ><tr><th>TerminalID</th><th>Reg No</th><th>Department</th><th>Date</th><th>Distance(km)</th></tr></td>";
            var idx = 0;

            //GpsMail.GetEmailIdsFromDB getEmails = new GpsMail.GetEmailIdsFromDB();
            EmailAlertComponent.GetData getdata = new EmailAlertComponent.GetData();
            getdata.connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;
            //For getting offline vehicle details
            getdata.storedProcName = "sp_data_emailreport_new";
            System.Data.DataSet ds = getdata.GetSPData(1);//1- for RAK Communifcation dept
            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                    {
                        
                        if (!string.IsNullOrEmpty(dr["Date"].ToString())) {
                            DateTime TodayDate = DateTime.Now.Date.AddDays(-1);
                            //DateTime TodayDate = Convert.ToDateTime(dr["Date"]);
                            DateTime RunningDate = Convert.ToDateTime(dr["Date"]);
                            if (TodayDate > RunningDate) {
                                bool even = idx % 2 == 0;
                                if (even)
                                {
                                    contests += "<tr style='background: #eee;'>";
                                }
                                else
                                {
                                    contests += "<tr style='background: #ffe;'>";
                                }
                                contests += "<tr style='background: #eee;'>";
                                contests += "<td style='padding: 6px;color:#000; 	border: 1px solid #ccc; text-align: left;' >" + dr["TerminalID"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["RegNo"] + "</td>";
                                contests += "<td style='padding: 6px; 	color:#000;border: 1px solid #ccc; text-align: left;'>" + dr["Name"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + TodayDate.ToString("dd-MMM-yyyy") + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'> 0 </td>";

                                contests += "</tr>";
                                idx++;
                            }
                            else
                            {
                                bool even = idx % 2 == 0;
                                if (even)
                                {
                                    contests += "<tr style='background: #eee;'>";
                                }
                                else
                                {
                                    contests += "<tr style='background: #ffe;'>";
                                }
                                contests += "<tr style='background: #eee;'>";
                                contests += "<td style='padding: 6px;color:#000; 	border: 1px solid #ccc; text-align: left;' >" + dr["TerminalID"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["RegNo"] + "</td>";
                                contests += "<td style='padding: 6px; 	color:#000;border: 1px solid #ccc; text-align: left;'>" + dr["Name"] + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + Convert.ToDateTime(dr["Date"]).ToString("dd-MMM-yyyy") + "</td>";
                                contests += "<td style='padding: 6px; color:#000;	border: 1px solid #ccc; text-align: left;'>" + dr["Distance"] + "</td>";

                                contests += "</tr>";
                                idx++;
                            }                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("R.A.K Communication :" + ex.Message.ToString());
                }

                contests += "</table>";
                string mail_text = "<p style='text-align:left;'>Hi,</p>";
                mail_text = mail_text + "<p style='text-align:left;'> Daily Report details for (" + DateTime.Now.Date.AddDays(-1).ToString("dd-MMM-yyyy") + ") are listed out in the following table. </p>";
                mail_text += "<div >" + contests + "</div><br>";
                //mail_text += "<p>Order registered by <b>" + db.users.FirstOrDefault(a => a.Id == SessionWrapper.UserID).Name + "</b></p>";
                mail_text += "<div>";
                mail_text += "<h4>Regards,</h4>";

                mail_text += "<p><strong>Thinture Technologies Pvt Ltd.,</strong><br>";

                mail_text += "";
                mail_text += "</div>";
                try
                {
                    getdata.storedProcName = "sp_data_getemails";
                    System.Data.DataSet email_ds = getdata.GetSPData(1);//1- for RAK Communifcation dept Email id
                    if (email_ds.Tables[0].Rows.Count > 0)
                    {
                        SendEmail(email_ds, "Daily Report", mail_text, "First", true);
                        Log.Info("R.A.K Communication: Mail sent successfully!");
                    }
                    else
                    {
                        Log.Info("R.A.K Communication: Mail ID not Available ");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("R.A.K Communication: " + ex.Message.ToString());
                }
            }
            else
            {
                Log.Info("R.A.K Communication: No data available");
            }
        }
        /// <summary>
        /// Temperature report for David
        /// </summary>
        public static void ServiceMailMethod_temp()
        {

            //  string contests;

            // contests = "<table style='width:100%;border-collapse: collapse; '>";
            //contests += "<thead style='background: #444; color: white;font-weight:bold;' ><tr><th>TerminalID</th><th>Reg No</th><th>Department</th><th>Date</th><th>Distance(km)</th></tr></td>";
            // var idx = 0;

            //GpsMail.GetEmailIdsFromDB getEmails = new GpsMail.GetEmailIdsFromDB();
            try
            {
                GetData getdata = new GetData();
                getdata.connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;
                //For getting offline vehicle details
                getdata.storedProcName = "sp_getScheduledVehicles";
                DataSet ds = getdata.GetSPData();

                foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                {

                    int id = Convert.ToInt32(dr["Id"]);
                    string sub = dr["RegNumber"].ToString() + "-" + DateTime.Now.Date.ToString("ddMMyyyy");

                    string terminal = dr["TerminalID"].ToString();
                    sendMail(sub, id, terminal, dr["RegNumber"].ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error("David Temp Report: " + ex.Message.ToString());
            }
        }

        private static void sendMail(string sub, int id, string terminal, string regno)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;

            string storedProcName = "sp_getScheduledMailIds";
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(storedProcName, conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Id", SqlDbType.Int).Value = id;

                SqlDataAdapter sda = new SqlDataAdapter(command);
                sda.Fill(ds);
            }
            string mail_text = "<p style='text-align:left;'>Please Find Attached</p>";
            Byte[] byt = ExportToMail(terminal);
            SendEmail(ds, sub, mail_text, "Second", true, byt, regno + "-" + DateTime.Now.Date.ToString("ddMMyyyy") + ".pdf");
            Log.Info("David : Mail sent successfully");
        }
        private static Byte[] ExportToMail(string ter)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SMSDB"].ConnectionString;

                string storedProcName = "sp_temperature_report_schedule";

                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(storedProcName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@TerminalID", SqlDbType.VarChar, 20).Value = ter;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(ds);
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfPTable table = new PdfPTable(6);
                    table.TotalWidth = 540f;
                    table.SpacingBefore = 20;

                    PdfPCell cell0 = new PdfPCell(new Phrase("Datetime", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell0.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell0);

                    PdfPCell cell6 = new PdfPCell(new Phrase("Temp1(ºC)", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell6.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell6);

                    PdfPCell cell7 = new PdfPCell(new Phrase("Temp2(ºC)", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell7);

                    PdfPCell cell2 = new PdfPCell(new Phrase("Event", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell2);
                    PdfPCell cell3 = new PdfPCell(new Phrase("Location", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell3);

                    PdfPCell cell5 = new PdfPCell(new Phrase("Door Status", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    cell5.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell5);

                    //PdfPCell durcell5 = new PdfPCell(new Phrase("Duration", new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD)));
                    //durcell5.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table.AddCell(durcell5);


                    Document pdfDoc = new Document(PageSize.A4, 7f, 7f, 7f, 0f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    int i = 0;
                    //     int count = list.Count();
                    //   DateTime strdate = DateTime.Now; DateTime endate = DateTime.Now;
                    string duration = "";
                    bool checkOpen = false;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        i = i + 1;
                        //rowdetails = item;

                        for (int j = 0; j < 6; j++)
                        {
                            if (j == 0)
                            {
                                var stdt = String.Format(CultureInfo.InstalledUICulture, "{0:dd/MMM/yyyy  hh:mm:ss tt}", Convert.ToDateTime(dr["IN_DATE"])).ToString();
                                table.AddCell(new Phrase(stdt, new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                            else if (j == 1)
                            {
                                table.AddCell(new Phrase(dr["temp1"].ToString(), new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                            else if (j == 2)
                            {
                                table.AddCell(new Phrase(dr["temp2"].ToString(), new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                            else if (j == 3)
                            {
                                var tmptxt = dr["evnt"].ToString() != "" ? dr["evnt"].ToString() : "Normal";
                                table.AddCell(new Phrase(tmptxt, new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                            else if (j == 4)
                            {
                                table.AddCell(new Phrase(dr["LOCATION"].ToString(), new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                            else if (j == 5)
                            {
                                table.AddCell(new Phrase(dr["Door"].ToString(), new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL)));
                            }
                        }
                    }

                    pdfDoc.Add(table);
                    pdfDoc.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    return bytes;
                }

            }
            catch (Exception ex)
            {
                Log.Error("David: " + ex.ToString());
                throw new FormatException(ex.ToString());
            }
        }

        public static void SendEmail(DataSet toAddress, string mailSubject, string mailBody, string mailOrder, bool isBodyHtml = false, Byte[] attachement = null, string attachmentName = null, bool isEnableSSL = false)
        {
            DataTable dt = toAddress.Tables[0];
            string reportSenderEmailId = ConfigurationManager.AppSettings["ReportSenderEmailId"].ToString();
            string reportSendingDisplayCompanyName = ConfigurationManager.AppSettings["ReportSendingDisplayCompanyName"].ToString();
            string reportSenderEmailPassword = ConfigurationManager.AppSettings["ReportSenderEmailPassword"].ToString();
            string emailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
            string emailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();
            string overrideEmailId = ConfigurationManager.AppSettings["OverrideEmailId"].ToString();
            string additionalCC = ConfigurationManager.AppSettings["AdditionalCC"].ToString();
            string testingBCC = ConfigurationManager.AppSettings["TestingBCC"].ToString();
            

            var fromAddress = new MailAddress(reportSenderEmailId, reportSendingDisplayCompanyName);

            string fromPassword = reportSenderEmailPassword;
            var smtp = new SmtpClient
            {
                Host = emailHost,
                Port = Convert.ToInt32(emailPort),
                EnableSsl = isEnableSSL,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            MailMessage message = new MailMessage();
            message.From = fromAddress;
            
            if (!string.IsNullOrEmpty(overrideEmailId))
            {
                message.To.Add(new MailAddress(overrideEmailId));
            }
            else
            {
                foreach (DataRow dr in toAddress.Tables[0].Rows)
                {
                    if (mailOrder == "First")
                    {
                        message.To.Add(new MailAddress(dr["email"].ToString(), dr["Name"].ToString()));
                    }
                    else
                    {
                        message.To.Add(new MailAddress(dr["MailId"].ToString(), dr["Name"].ToString()));
                    }
                }
                if (!string.IsNullOrEmpty(additionalCC))
                {
                    message.CC.Add(new MailAddress(additionalCC));
                }
                if (!string.IsNullOrEmpty(testingBCC))
                {
                    message.Bcc.Add(new MailAddress(testingBCC));
                }
            }
            message.Subject = mailSubject;
            if (attachement != null)
            {
                message.Attachments.Add(new Attachment(new MemoryStream(attachement), attachmentName));
            }

            message.Body = mailBody;
            message.IsBodyHtml = isBodyHtml;
            if (message.To.Count > 0)
            {
                smtp.Send(message);
            }
            message.Dispose();
        }
    }
}
